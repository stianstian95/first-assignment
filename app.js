// Bank section
const balanceElement = document.getElementById("balance");
const addBalanceButtonElement = document.getElementById("addBalanceButton");
const loanBalanceElement = document.getElementById("loanBalance");
const repayLoanElement = document.getElementById("repayLoan");
// Work section
const payElement = document.getElementById("pay");
const bankButtonElement = document.getElementById("bankButton");
const workButtonElement = document.getElementById("workButton");
// laptop section
const laptopSelectElement = document.getElementById("laptop");
const descriptionElement = document.getElementById("description");
// View section
const priceElement = document.getElementById("price");
const buyButtonElement = document.getElementById("buyButton");
const titleElement = document.getElementById("title");
const specsElement = document.getElementById("specs");
const stockElement = document.getElementById("stock");
const activeElement = document.getElementById("active");
const imageElement = document.getElementById("image");

// Variables
let laptops = [];
let balance = 200.0;
let payBalance = 0.0;
let dues = 0.0;
let currentPc = '';
let currentPrice = 0;
let url = 'https://noroff-komputer-store-api.herokuapp.com/';

// API fetch
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToShop(laptops));

// adds laptops
const addLaptopsToShop = (laptops) => {
    laptops.forEach(x => addLaptopToShop(x));
    descriptionElement.innerText = laptops[0].description;
    priceElement.innerText = laptops[0].price;
    specsElement.innerText = laptops[0].specs;
    stockElement.innerText = laptops[0].stock;
    activeElement.innerText = laptops[0].active;
    urlPath = laptops[0].image;
    imageElement.src = url + urlPath;

    currentPc = laptops[0].title;
    currentPrice = laptops[0].price;
}

// add laptop with titles
const addLaptopToShop = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopSelectElement.appendChild(laptopElement);
}

// handles UI when laptop is selected
const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    descriptionElement.innerText = selectedLaptop.description;
    priceElement.innerText = selectedLaptop.price;
    specsElement.innerText = selectedLaptop.specs;
    stockElement.innerText = selectedLaptop.stock;
    activeElement.innerText = selectedLaptop.active;
    urlPath = selectedLaptop.image;
    imageElement.src = url + urlPath;

    currentPc = selectedLaptop.title;
    currentPrice = selectedLaptop.price;
}

// Adds funds to the balance by issuing a loan
const handleAddBalance = () => {
    const loan = prompt("Please enter the amount of money you wish to loan: ");

    if (dues != 0.0) {
        alert("You have unpaid loans already");
    }
    else {
        if(loan < balance*2){
            balance += parseFloat(loan);
            dues = parseFloat(loan);
            balanceElement.innerText = balance;
            loanBalanceElement.innerText = "Outstanding loan amount: " + dues;
            document.getElementById("repayLoan").style.visibility = "visible";
        }
        else{
            alert("This is an unacceptable amount");
        }
    }
}

//bank & work
const handleBankButton = () => {
    if(dues != 0) {
        deductSalary = payBalance * .1;
        dues = dues - deductSalary;
        balance = balance + payBalance - deductSalary;
    } else {
        balance += payBalance;
    }
    payBalance = 0;
    payElement.innerText = payBalance;
    balanceElement.innerText = balance;
    loanBalanceElement.innerText = "Loan - " + dues;
}

// Adds a "salary"
const handleWorkButton = () => {
    payBalance += 100;
    payElement.innerText = payBalance;
    console.log(payBalance);
}

// lets one "buy" and displays PC and remaining funds
const handleBuyButton = () => {
    if(currentPrice <= balance) {
        balance -= currentPrice
        balanceElement.innerText = balance;
        alert("Congratulations you now own " + currentPc + "Your funds are now: " + balance);
    } else {
        alert("You cant afford this");
    }
}

// repays debt
const handleRepayLoan = () => {
    balance = balance - dues;
    dues = 0;
    balanceElement.innerText = balance;
    loanBalanceElement.innerText = dues;
}

// Events
addBalanceButtonElement.addEventListener("click", handleAddBalance);
bankButtonElement.addEventListener("click", handleBankButton);
workButtonElement.addEventListener("click", handleWorkButton);
laptopSelectElement.addEventListener("change", handleLaptopChange);
buyButtonElement.addEventListener("click", handleBuyButton);
repayLoanElement.addEventListener("click", handleRepayLoan);
